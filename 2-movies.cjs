const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}



// NOTE: For all questions, the returned data must contain all the movie information including its name.


// Q1. Find all the movies with total earnings more than $500M. 

const moviesTotalEarningsGreaterThan$500M = Object.fromEntries(Object.entries(favouritesMovies).filter(([movieName, movieDetails]) => {
    let movieTotalEarnings = Number(movieDetails.totalEarnings.replace("$", "").replace("M", ""))
    return (movieTotalEarnings > 500)
})
);
console.log(moviesTotalEarningsGreaterThan$500M);

// Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.

const moviesMoreThan3OscarNominationsAndTotalEarningsGreaterThan$500M = Object.fromEntries(Object.entries(moviesTotalEarningsGreaterThan$500M).filter(([movieName, movieDetails]) => {

    return (movieDetails.oscarNominations > 3)
})
);
console.log(moviesMoreThan3OscarNominationsAndTotalEarningsGreaterThan$500M);

// Q.3 Find all movies of the actor "Leonardo Dicaprio".

const moviesWithLeonardoDicaprio = Object.fromEntries(Object.entries(favouritesMovies).filter(([movieName, movieDetails]) => {

    return (movieDetails.actors.includes("Leonardo Dicaprio"))
})
);
console.log(moviesWithLeonardoDicaprio);

// Q.4 Sort movies (based on IMDB rating)
//     if IMDB ratings are same, compare totalEarning as the secondary metric.

const sortedMoviesByIMDBRating = Object.fromEntries(Object.entries(favouritesMovies).sort(([movieName1, movieDetails1], [movieName2, movieDetails2]) => {

    let compareByIMDBRating = movieDetails2.imdbRating - movieDetails1.imdbRating
    return compareByIMDBRating != 0
        ? compareByIMDBRating
        : (Number(movieDetails2.totalEarnings.replace("$", "").replace("M", "")) - Number(movieDetails1.totalEarnings.replace("$", "").replace("M", "")))
})
);
console.log(sortedMoviesByIMDBRating);

// Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
//     drama > sci-fi > adventure > thriller > crime

function getHighestGenre(movieGenre) {
    const genresValue = {
        "drama": 5,
        "sci-fi": 4,
        "adventure": 3,
        "thriller": 2,
        "crime": 1
    }
    return Object.keys(genresValue)
        .reduce((acc, genre) => {
            return (movieGenre.includes(genre)) ?
                (acc != "" && genresValue[acc] > genresValue[genre]) ? acc : genre
                : acc
        }
            , "")

}



const usersGroupedByGenre = Object.entries(favouritesMovies).reduce((groupByGenre, [movieName, movieDetails]) => {
    movieGenre = getHighestGenre(movieDetails.genre)
    // console.log(movieGenre)
    if (groupByGenre[movieGenre] === undefined) {
        groupByGenre[movieGenre] = []
    }
    groupByGenre[movieGenre].push(movieName)
    return groupByGenre
}, {})

console.log(usersGroupedByGenre)



// NOTE: Do not change the name of this file






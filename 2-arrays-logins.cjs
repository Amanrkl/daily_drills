const persons =[{"id":1,"first_name":"Valera","last_name":"Pinsent","email":"vpinsent0@google.co.jp","gender":"Male","ip_address":"253.171.63.171"},
{"id":2,"first_name":"Kenneth","last_name":"Hinemoor","email":"khinemoor1@yellowbook.com","gender":"Polygender","ip_address":"50.231.58.150"},
{"id":3,"first_name":"Roman","last_name":"Sedcole","email":"rsedcole2@addtoany.com","gender":"Genderqueer","ip_address":"236.52.184.83"},
{"id":4,"first_name":"Lind","last_name":"Ladyman","email":"lladyman3@wordpress.org","gender":"Male","ip_address":"118.12.213.144"},
{"id":5,"first_name":"Jocelyne","last_name":"Casse","email":"jcasse4@ehow.com","gender":"Agender","ip_address":"176.202.254.113"},
{"id":6,"first_name":"Stafford","last_name":"Dandy","email":"sdandy5@exblog.jp","gender":"Female","ip_address":"111.139.161.143"},
{"id":7,"first_name":"Jeramey","last_name":"Sweetsur","email":"jsweetsur6@youtube.com","gender":"Genderqueer","ip_address":"196.247.246.106"},
{"id":8,"first_name":"Anna-diane","last_name":"Wingar","email":"awingar7@auda.org.au","gender":"Agender","ip_address":"148.229.65.98"},
{"id":9,"first_name":"Cherianne","last_name":"Rantoul","email":"crantoul8@craigslist.org","gender":"Genderfluid","ip_address":"141.40.134.234"},
{"id":10,"first_name":"Nico","last_name":"Dunstall","email":"ndunstall9@technorati.com","gender":"Female","ip_address":"37.12.213.144"}]



// 1. Find all people who are Agender
const allAgender= persons.filter((person) => person.gender.includes("Agender"));

console.log(allAgender);


// 2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]

const allIPAddress = persons.map((person) => person.ip_address.split(".")
                                                    .map((component) => Number(component))
                            );

console.log(allIPAddress);


// 3. Find the sum of all the second components of the ip addresses.

const sumOfSecondComponent = allIPAddress.reduce((sumValue, componentsValue) => sumValue + componentsValue[1] , 0);

console.log(sumOfSecondComponent);

// 4. Find the sum of all the fourth components of the ip addresses.

const sumOfFourthComponent = allIPAddress.reduce((sumValue, componentsValue) => sumValue + componentsValue[3] , 0);

console.log(sumOfFourthComponent);

// 5. Compute the full name of each person and store it in a new key (full_name or something) for each person.

const personsFullName = persons.map((person) => {
                                person.full_name = `${person.first_name} ${person.last_name}`
                                return person.full_name});

console.log(personsFullName);


// 6. Filter out all the .org emails

const allOrgEmails = persons.filter((person) => person.email.endsWith(".org"))
                            .map(person => person.email);

console.log(allOrgEmails);


// 7. Calculate how many .org, .au, .com emails are there

initialEmailsPerTLD ={
    org :0,
    au :0,
    com: 0
}

const noOfemailsPerTLD = persons.reduce((emailsPerTLD, person) =>{
                                        if (person.email.endsWith(".org")){
                                            emailsPerTLD.org +=1
                                        }else if (person.email.endsWith(".au")){
                                            emailsPerTLD.au +=1
                                        }if (person.email.endsWith(".com")){
                                            emailsPerTLD.com +=1
                                        }
                                        return emailsPerTLD}, initialEmailsPerTLD);

console.log(noOfemailsPerTLD);

// 8. Sort the data in descending order of first name

const sortedOnFirstName = persons.sort((person1, person2) => {
                                        if (person1.first_name.toUpperCase() < person2.first_name.toUpperCase()){
                                            return 1
                                        }
                                        else if (person1.first_name.toUpperCase() > person2.first_name.toUpperCase()){
                                            return -1
                                        }
                                        else {
                                            return ((person1.last_name.toUpperCase() < person2.last_name.toUpperCase()) ? 1 : -1)
                                        }
                                    });

console.log(sortedOnFirstName);



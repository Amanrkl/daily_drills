const products = [
    {
        shampoo: {
            price: "$50",
            quantity: 4,
        },
        "Hair-oil": {
            price: "$40",
            quantity: 2,
            sealed: true,
        },
        comb: {
            price: "$12",
            quantity: 1,
        },
        utensils: [
            {
                spoons: { quantity: 2, price: "$8" },
            },
            {
                glasses: { quantity: 1, price: "$70", type: "fragile" },
            },
            {
                cooker: { quantity: 4, price: "$900" },
            },
        ],
        watch: {
            price: "$800",
            quantity: 1,
            type: "fragile",
        },
    },
];

// Q1. Find all the items with price more than $65.

function itemsWithPriceMoreThan$65(products) {
    return products.reduce((items, product) => {
        items = Object.entries(product).reduce((items, [item, itemDetails]) => {
            if (Array.isArray(itemDetails)) {
                items = items.concat(itemsWithPriceMoreThan$65(itemDetails));
            } else {
                const currPrice = Number(itemDetails.price.replace("$", ""),);
                if (currPrice > 65) {
                    items.push({[item] : itemDetails})
                };
            }
            return items
        }, items);
        return items;

    }, []);
}

// console.log(itemsWithPriceMoreThan$65(products));

// Q2. Find all the items where quantity ordered is more than 1.

function itemsWithQuantityMoreThan1(products) {
    return products.reduce((items, product) => {
        items = Object.entries(product).reduce((items, [item, itemDetails]) => {
            if (Array.isArray(itemDetails)) {
                items = items.concat(itemsWithQuantityMoreThan1(itemDetails));
            } else {
                if (itemDetails.quantity > 1) {
                    items.push({[item] : itemDetails})
                };
            }
            return items
        }, items);
        return items;

    }, []);
}
    

// console.log(itemsWithQuantityMoreThan1(products));

// Q.3 Get all items which are mentioned as fragile.

function fragileItems(products) {
    return products.reduce((items, product) => {
        items = Object.entries(product).reduce((items, [item, itemDetails]) => {
            if (Array.isArray(itemDetails)) {
                items = items.concat(fragileItems(itemDetails));
            } else {
                if ((itemDetails.hasOwnProperty("type")) && itemDetails.type == "fragile" ) {
                    items.push({[item] : itemDetails})
                };
            }
            return items
        }, items);
        return items;

    }, []);
    
}

console.log(fragileItems(products));

// Q.4 Find the least and the most expensive item for a single quantity.

// console.log(allItems(products));



// Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

// NOTE: Do not change the name of this file

const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interest: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}




// Q1 Find all users who are interested in playing video games.

const usersInterestedInVideoGames = Object.fromEntries(Object.entries(users).filter(([userName, userDetails]) => {
    let interestsOfUser = (userDetails.interests ?? userDetails.interest)[0]
    return (interestsOfUser.includes("Video Games"))
})
);
// console.log(usersInterestedInVideoGames);




// Q2 Find all users staying in Germany.

const usersStayingInGermany = Object.fromEntries(Object.entries(users).filter(([userName, userDetails]) => {
    return (userDetails.nationality.trim() === "Germany")
})
);
// console.log(usersStayingInGermany);




// Q3 Sort users based on their seniority level 
//    for Designation - Senior Developer > Developer > Intern
//    for Age - 20 > 10

function designationValue(designation) {
    const designationValue = {
        "Senior": 3,
        "Developer": 2,
        "Intern": 1
    }
    return Object.entries(designationValue).reduce((acc, [title, titleValue]) => acc + (designation.includes(title) ? titleValue : 0), 0)

}

const usersSortedByDesignation = Object.fromEntries(Object.entries(users).sort(([userName1, userDetails1], [userName2, userDetails2]) => {

    let compare = designationValue(userDetails2.desgination) - designationValue(userDetails1.desgination)
    return compare != 0 ? compare : (userDetails2.age - userDetails1.age)
})
);

// console.log(usersSortedByDesignation);




// Q4 Find all users with masters Degree.

const usersWithMasters = Object.fromEntries(Object.entries(users).filter(([userName, userDetails]) => {
    return (userDetails.qualification.includes("Masters"))
})
);
// console.log(usersWithMasters);



// Q5 Group users based on their Programming language mentioned in their designation.

function getLanguage(word) {
    wordsToRemove = ["Senior", "Developer", "Intern", " ", "-"]
    let language = wordsToRemove.reduce((language, wordToRemove) => language.replace(wordToRemove, ""), word)
    return language
}



const usersGroupedByLanguage = Object.entries(users).reduce((groupByLanguage, [userName, userDetails]) => {
    userLanguage = getLanguage(userDetails.desgination).trim()
    if (groupByLanguage[userLanguage] === undefined) {
        groupByLanguage[userLanguage] = []
    }
    groupByLanguage[userLanguage].push(userName)
    return groupByLanguage
}, {})

console.log(usersGroupedByLanguage)


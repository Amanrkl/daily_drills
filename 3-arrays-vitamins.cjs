const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


                           

// 1. Get all items that are available 

const allAvaiableItems = items.filter((item) => item.available);                                                 

console.log(allAvaiableItems);

// 2. Get all items containing only Vitamin C.

const onlyVitaminCItems = items.filter((item) => item.contains === "Vitamin C");                                                 

console.log(onlyVitaminCItems);


// 3. Get all items containing Vitamin A.


const vitaminAItems = items.filter((item) => item.contains.includes("Vitamin A"));                                                 

console.log(vitaminAItems);


// 4. Group items based on the Vitamins that they contain in the following format:
//     {
//         "Vitamin C": ["Orange", "Mango"],
//         "Vitamin K": ["Mango"],
//     }
        
//     and so on for all items and all Vitamins.

const itemsGroupByVitamins = items.reduce((vitaminGroup, item) => {
    let vitamins = item.contains.split(", ");
    vitamins.forEach((vitamin) => {
        if (vitaminGroup[vitamin] === undefined){
            vitaminGroup[vitamin] = []
        }
        vitaminGroup[vitamin].push(item.name)
    })
    return vitaminGroup},{});

console.log(itemsGroupByVitamins);


// 5. Sort items based on number of Vitamins they contain.


const sortedItemsByNoOfVitamins = items.sort((item1,item2) => {
    let vitamins1 = item1.contains.split(", ");
    let vitamins2 = item2.contains.split(", ");
    return (vitamins1.length > vitamins2.length ) ? 1 : (vitamins1.length < vitamins2.length) ? -1 : (item1.name.toUpperCase() < item2.name.toUpperCase) ? 1 :-1    
});

console.log(sortedItemsByNoOfVitamins);


